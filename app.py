from endpoints import start_app
from mqtt_service import MqttService

if __name__ == '__main__':
    print("Press 1 for miner, press 2 for blockchain")
    if str(input()) == "1":
        print("Activating miner.")
        MqttService()
    else:
        start_app()
