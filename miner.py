import requests


class Miner(object):
    base_url = 'http://10.0.0.149:1337'
    node_register_url = '/nodes/register'
    miner_url = '/mine'
    transaction = '/transactions/new'
    headers = {"Content-Type": "application/json"}

    def register_node(self):
        try:
            body = '{"nodes": ["http://127.0.0.1:1338"]}'
            response = requests.post(self.base_url + self.node_register_url, data=body,
                                     headers=self.headers)
            print(response.text)
        except:
            print("Nodes not added.")

    def add_block(self):
        try:
            return requests.get(self.base_url + self.miner_url).text
        except:
            print("new block not added.")

    def new_transaction(self, body):
        try:
            response = requests.post(self.base_url + self.transaction, data=body, headers=self.headers)
            print(response.text)
        except:
            print("new transaction not added.")
