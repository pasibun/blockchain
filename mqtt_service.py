import paho.mqtt.client as mqtt
from miner import Miner
import json
from random import randint


class MqttService(object):
    client = mqtt.Client("blockchain")
    miner = Miner()
    MQTT_HOST = "10.0.0.109"
    MQTT_USERNAME = ""
    MQTT_PASSWORD = ""
    MQTT_TOPIC = "#"

    def __init__(self):
        print("init mqtt service")
        self.make_connection()
        self.subscribe_to_topics()
        self.miner.register_node()
        self.client.loop_forever()  # loop_start

    def make_connection(self):
        self.get_credentials()
        print("Making connection with mqtt service.")
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.username_pw_set(username=self.MQTT_USERNAME, password=self.MQTT_PASSWORD)
        self.client.connected_flag = False
        self.client.connect(self.MQTT_HOST, port=1883, keepalive=60, bind_address="")

    def get_credentials(self):
        try:
            print("Enter username\nMQTT connection:")
            r = open("/home/pi/file.txt", "r")
            self.MQTT_USERNAME = str(r.readline().strip())
            print("Enter password\nMQTT connection:")
            self.MQTT_PASSWORD = str(r.readline().strip())
        except ValueError:
            print("Wrong fucking\ninput retard.")

    def subscribe_to_topics(self):
        print("Subscribing to topic: ", self.MQTT_TOPIC)
        self.client.subscribe(self.MQTT_TOPIC)

    def on_connect(self, client, userdata, flags, rc):
        if rc == 0:
            self.client.connected_flag = True
            print("connected ok")
        else:
            print("Bad connection Returned code= ", rc)

    def on_message(self, client, userdata, message):
        payload = str(message.payload.decode("utf-8"))
        print("Topic: " + message.topic + ". Message received ", payload)
        body = {'topic': message.topic, 'payload': payload}
        self.miner.new_transaction(json.dumps(body))
        value = randint(0, 50)
        if value == 1:
            print("miner activated..")
            self.miner.add_block()
